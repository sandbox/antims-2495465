<?php

/**
 * views_field_orderby_pinyin 继承views sort handler类，并对query()、option_definition()、options_form()三个函数进行覆写.
 * Extends the views_handler_sort class to provide a Hanyu Pinyin ORDER BY FIELD() option to orderby
 */
class views_field_orderby_pinyin extends views_handler_sort {
	function query() {
	        $this->ensure_my_table();
		$field = $this->view->field[$this->options['field_orderby_pinyin']];
                $this->query->add_table($field->table);
         	$this->query->orderby[] = array(
		   'field' => "Convert({$field->table}.{$field->real_field} USING GBK)",
		   'direction' => $this->options['order'],
		 );
	}
	function option_definition() {
            $options = parent::option_definition();
	    $options['field_orderby_pinyin'] = array('default' => '');
            return $options;
        }

	function options_form(&$form, &$form_state) {
		parent::options_form($form, $form_state);
		$this->view->build();
		$fields = array();
		foreach($this->view->field as $field_name => $field_data) {
			$fields[$field_name] = $field_name;
		}
 
  // 选择哪个字段需要用汉语拼音排序
            $form['field_orderby_pinyin'] = array(
              '#title' => t('Field OrderBy Pinyin'),
              '#type' => 'select',
              '#options' => $fields,
              '#default_value' => $this->options['field_orderby_pinyin'],
              '#required' => TRUE,
    );
	}

}
